//
//  ViewController.swift
//  Rainbow
//
//  Created by VJ Pranay on 12/06/18.
//  Copyright © 2018 Lamda School. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBAction func green(_ sender: Any) {
        view.backgroundColor = UIColor(red:0.37, green:0.74, blue:0.24, alpha:1.0)
    }
    
    @IBAction func yellow(_ sender: Any) {
        view.backgroundColor = UIColor(red:1.00, green:0.73, blue:0.00, alpha:1.0)
    }
    
    @IBAction func orange(_ sender: Any) {
        view.backgroundColor = UIColor(red:0.97, green:0.51, blue:0.00, alpha:1.0)
    }
    
    @IBAction func read(_ sender: Any) {
        view.backgroundColor = UIColor(red:0.89, green:0.22, blue:0.22, alpha:1.0)
    }
    
    @IBAction func violet(_ sender: Any) {
        view.backgroundColor = UIColor(red:0.59, green:0.22, blue:0.60, alpha:1.0)
    }
    
    @IBAction func blue(_ sender: Any) {
        view.backgroundColor = UIColor(red:0.00, green:0.61, blue:0.87, alpha:1.0)
    }
    
}

